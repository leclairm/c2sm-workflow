# Workflow Tools

## Comparing AiiDA with cylc

| AiiDA                                                 | Cylc                                                   |
|-------------------------------------------------------|--------------------------------------------------------|
| + officialy supported by CSCS                         | + used and developed by weather and climate services   |
| + actively developed by EPFL / PSI                    | + easy learning curve, intuitive to use                |
| + Empa already has first experiences                  | + Anurag successfully used it in the past              |
| + lots of training material available                 | + powerful GUIs                                        |
|                                                       | + ready to use                                         |
|                                                       | + decentralized nature                                 |
|                                                       | -> control workflows from laptop or a central instance |
|                                                       | -> run tasks of a workflow on different machines       |
|                                                       | + ability to run small tasks in the background         |
|                                                       | + responsive support team (tested with an issue)       |
|                                                       | + possibility for MCH to use it operationally          |
|                                                       |                                                        |
| - further developments needed until usable for W&C    | - maintained by 3rd party                              |
| - difficult learning curve                            |                                                        |

